const express = require('express');
const conectarDB  = require('./config/db');
const cors = require('cors');

// Crear el servidor
const app = express();

// Conectar a la base de datos
conectarDB();

// Habilitar Cors
app.use(cors());

// Habilitar express.json
app.use( express.json({ extended: true }));

// puerto de la app
const PORT = process.env.PORT || 4000;

// Rutas
app.use('/api/tareas', require('./routes/tareas'));


// arrancando la app
app.listen(PORT, () => {
    console.log(`Servidor corriendo en el puerto ${PORT}`);
});