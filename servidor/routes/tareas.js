const express = require('express');
const router = express.Router();
const tareaController = require('../controllers/tareaController');
const { check } = require('express-validator');

// Crear una tarea
// api/tarea
router.post('/',
    [
        check('nombre', 'El Nombre es obligatorio').not().isEmpty(),
    ],
    tareaController.crearTarea
);

// Obtener las tareas
router.get('/',
    tareaController.obtenerTareas
);

// Actualizar tarea
router.put('/:id',
    tareaController.actualizarTarea
);

// Eliminar tarea
router.delete('/:id',
    tareaController.eliminarTarea
);

module.exports = router;