const Tarea = require('../models/Tarea');
const { validationResult } = require('express-validator');

// Crear una nueva tarea
exports.crearTarea = async (req, res) => {

     // Revisar si hay errores
    const errores = validationResult(req);
    if( !errores.isEmpty() ) {
        return res.status(400).json({errores: errores.array() })
    }

    try {
        // Crear un nuevo tarea
        const tarea = new Tarea(req.body);
        tarea.save();
        res.json(tarea);
        
    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }

}

// Obtiene las tareas
exports.obtenerTareas = async (req, res) => {
    
    try {
        // obtener tareas
        const tareas = await Tarea.find().sort({creado: -1});
        res.json(tareas);
        
    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }
}

// Actualizar una tarea
exports.actualizarTarea = async (req, res) => {
    
    try {
        // extraer datos
        const { nombre, estado } = req.body;
        
        // Si la tarea existe
        let tarea = await Tarea.findById(req.params.id);

        if(!tarea) {
            return res.status(404).json({msg: 'No existe esa tarea'});
        }

        // crear un objeto con la nueva informacion
        const nuevaTarea = {};
        nuevaTarea.nombre = nombre;
        nuevaTarea.estado = estado;

        // Guardar tarea
        tarea = await Tarea.findOneAndUpdate({_id : req.params.id},
            nuevaTarea, {new: true} );

        res.json({tarea});
        
    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }
}

// ELiminar tarea
exports.eliminarTarea = async (req, res) => {

    try {
       
        // Si la tarea existe
        let tarea = await Tarea.findById(req.params.id);

        if(!tarea) {
            return res.status(404).json({msg: 'No existe esa tarea'});
        }

        // eliminar
        await Tarea.findOneAndRemove({_id: req.params.id});
        res.json({msg: 'Tarea Eliminada'});
        
    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }
}