const moongoose = require('mongoose');
require('dotenv').config({ path: 'variables.env' });

const conectarDB = async () => {
    try {
        await moongoose.connect(process.env.DB_MONGO, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        });
        console.log('Conectado a la base de datos');
    } catch (error) {
        console.error(error);
        process.exit(1); // Detiene la app
    }
}

module.exports = conectarDB;