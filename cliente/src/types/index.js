export const OBTENER_TAREAS = 'OBTENER_TAREAS';
export const AGREGAR_TAREA = 'AGREGAR_TAREA';
export const ELIMINAR_TAREA = 'ELIMINAR_TAREA';
export const TAREA_ACTUAL = 'TAREA_ACTUAL';
export const ACTUALIZAR_TAREA = 'ACTUALIZAR_TAREA';
export const LIMPIAR_TAREA = 'LIMPIAR_TAREA';