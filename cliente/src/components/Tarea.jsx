import React, { useContext } from 'react';
import TareaContext from '../context/tareaContext';

const Tarea = ({tarea}) => {

    // obtener la funcion del state de tarea
    const tareasContext = useContext(TareaContext);
    const { eliminarTarea, obtenerTareas, actualizarTarea, guardarTareaActual } = tareasContext;

    // funcion que se ejecuta al eliminar una tarea
    const tareaEliminar = id => {
        eliminarTarea(id);
        obtenerTareas();
    }

    // uncion que modifica el estado de la tareea
    const cambiarEstado = tarea => {
        if(tarea.estado) {
            tarea.estado = false;
        } else {
            tarea.estado = true;
        }

        actualizarTarea(tarea);
    }

    // Agrega una tarea actual cuando es seleccionada para editarla
    const seleccionarTarea = tarea => {
        guardarTareaActual(tarea);
    }

    return ( 
        <li className="tarea sombra">
            <p>{tarea.nombre}</p>

            <div className="estado">
                { tarea.estado

                ?   ( 
                        <button
                            type="button"
                            className="completo"
                            onClick={ () => cambiarEstado(tarea) }
                        >Completo</button>
                    )
                :

                    ( 
                        <button
                            type="button"
                            className="incompleto"
                            onClick={ () => cambiarEstado(tarea) }
                        >Pendiente</button>
                    )

                }
            </div>

            <div className="acciones">
                <button
                    type="button"
                    className="btn btn-primario"
                    onClick={ () => seleccionarTarea(tarea) }
                >Editar</button>

                <button
                    type="button"
                    className="btn btn-secundario"
                    onClick={() => tareaEliminar(tarea._id)}
                >Eliminar</button>
            </div>
        </li>
    );
}
 
export default Tarea;