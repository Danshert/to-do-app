import React, { Fragment, useContext, useEffect } from 'react';

import Tarea from './Tarea';

import TareaContext from '../context/tareaContext';

const ListadoTareas = () => {

    // obtener las tareas del proyecto
    const tareasContext = useContext(TareaContext);
    const { tareasLista, obtenerTareas } = tareasContext;

    useEffect(() => {
        obtenerTareas();
        // eslint-disable-next-line
    }, [])

    return ( 
        <Fragment>
            <br/>
            <h2>Listado de Tareas</h2>
            <ul className="listado-tareas">
                { tareasLista.length === 0
                    ? (<li className="tarea"><p>No hay tareas</p></li>)
                    : tareasLista.map(tarea => (
                        <Tarea
                            key={tarea._id} 
                            tarea={tarea}
                        />
                    ))
                }
                
            </ul>
        </Fragment>
    );
}
 
export default ListadoTareas;