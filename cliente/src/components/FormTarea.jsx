import React, { useContext, useState, useEffect } from 'react';
import TareaContext from '../context/tareaContext';

const FormTarea = () => {

    // obtener las funciones de tareas
    const tareasContext = useContext(TareaContext);
    const { tareaseleccionada, agregarTarea, obtenerTareas, actualizarTarea, limpiarTarea  } = tareasContext;

    // Effect que tecta si hay una tarea seleccionada
    useEffect(() => {

        if (tareaseleccionada !== null) {
            guardarTarea(tareaseleccionada);
        } else {
            guardarTarea({
                nombre: ''
            });
        }
    }, [tareaseleccionada]);

    // State del formulario
    const [tarea, guardarTarea] = useState({
        nombre: ''
    })

    // extraer el nombre
    const { nombre } = tarea;

    // lee los valores del formulario
    const handleChange = e => {
        guardarTarea({
            ...tarea,
            [e.target.name] : e.target.value
        })
    }

    const onSubmit = e => {
        e.preventDefault();

        // validar
        if (nombre.trim() === '') {
            return;
        }

        // Revisar si se esta editando o se esta creando una tarea
        if (tareaseleccionada === null) {
            // agregar la nueva tarea al state
            agregarTarea(tarea);
        } else {
            actualizarTarea(tarea);
            limpiarTarea();
        }

        // obtener las tareas
        obtenerTareas();
        

        // reiniciar el form
        guardarTarea({
            nombre: ''
        });
    }


    return ( 
        <div className="formulario">
            <h1>To Do App</h1>
            <form
                onSubmit={onSubmit}
            >
                <div className="contenedor-input">
                    <input
                        type="text"
                        className="input-text"
                        placeholder="Nombre de la tarea"
                        name="nombre"
                        value={nombre}
                        onChange={handleChange}
                    />
                </div>

                <div className="contenedor-input">
                    <input
                        type="submit"
                        className="btn btn-primario btn-submit btn-block"
                        value={tareaseleccionada ? 'Editar Tarea' : 'Agregar Tarea'}
                    />
                </div>

            </form>
        </div>
    );
}
 
export default FormTarea;