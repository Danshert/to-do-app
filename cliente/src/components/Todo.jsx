import React from 'react';

import TareaState from '../context/tareaState';

import FormTarea from '../components/FormTarea';
import ListadoTareas from '../components/ListadoTareas';

const Todo = () => {

    return ( 
        <TareaState>
            <FormTarea />
            <ListadoTareas />
        </TareaState>
    );
}
 
export default Todo;