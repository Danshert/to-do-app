import {
    OBTENER_TAREAS,
    AGREGAR_TAREA,
    ELIMINAR_TAREA,
    TAREA_ACTUAL,
    ACTUALIZAR_TAREA,
    LIMPIAR_TAREA

} from '../types';

export default (state, action) => {
    switch (action.type) {

        case OBTENER_TAREAS:
            return {
                ...state,
                tareasLista: action.payload
            }

        case AGREGAR_TAREA:
            return {
                ...state,
                tareasLista: [action.payload, ...state.tareasLista]
            }

        case ELIMINAR_TAREA:
            return {
                ...state,
                tareasLista: state.tareasLista.filter(tarea => tarea._id !== action.payload)
            }

        case ACTUALIZAR_TAREA:
            return {
                ...state,
                tareasLista: state.tareasLista.map(tarea => tarea._id === action.payload._id ? 
                    action.payload : tarea)
            }
        
        case TAREA_ACTUAL:
            return {
                ...state,
                tareaseleccionada: action.payload
            }

        case LIMPIAR_TAREA:
            return {
                ...state,
                tareaseleccionada: null
            }

        default:
            return state;
    }
}