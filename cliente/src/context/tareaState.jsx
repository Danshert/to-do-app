import React, { useReducer, useEffect } from 'react';
import TareaContext from './tareaContext';
import TareaReducer from './tareaReducer';

import {
    OBTENER_TAREAS,
    AGREGAR_TAREA,
    ELIMINAR_TAREA,
    TAREA_ACTUAL,
    ACTUALIZAR_TAREA,
    LIMPIAR_TAREA
} from '../types';

import clienteAxios from '../config/axios';

const TareaState = props => {

    useEffect(() => {
        obtenerTareas();
        // eslint-disable-next-line
    }, [])

    const initialState = {
        tareasLista: [],
        tareaseleccionada: null
    }

    // rear dispatch y state
    const [state, dispatch] = useReducer(TareaReducer, initialState);

    // obtener las tareas
    const obtenerTareas = async () => {
        
        try {
            const resultado = await clienteAxios.get('/api/tareas');
            // console.log(resultado);
            
            dispatch({
                type: OBTENER_TAREAS,
                payload: resultado.data
            });
        } catch (error) {
            console.log(error);
        }
    }

    // Agregar una tarea
    const agregarTarea = async tarea => {
        
        try {
            const resultado = await clienteAxios.post('/api/tareas', tarea);
            console.log(resultado);
            
            dispatch({
                type: AGREGAR_TAREA,
                payload: tarea
            });
        } catch (error) {
            console.log(error);
        }
    };

    // Eliminar tarea
    const eliminarTarea = async id => {
        try {
            await clienteAxios.delete(`/api/tareas/${id}`);
            dispatch({
                type: ELIMINAR_TAREA,
                payload: id
            });
        } catch (error) {
            console.log(error);
        }
    };

      // Editar una tarea
    const actualizarTarea = async tarea => {
        try {
            const resultado = await clienteAxios.put(`/api/tareas/${tarea._id}`, tarea);
            console.log(resultado);
            
            
            dispatch({
                type: ACTUALIZAR_TAREA,
                payload: resultado.data
            });
        } catch (error) {
            console.log(error);
        }
    };

    // Extrae una tarea para edicion
    const guardarTareaActual = tarea => {
        dispatch({
            type: TAREA_ACTUAL,
            payload: tarea
        });
    };


    // Elimina la tarea seleccionada
    const limpiarTarea = () => {
        dispatch({
            type: LIMPIAR_TAREA
        })
    }

    return (
        <TareaContext.Provider
            value={{
                tareasLista: state.tareasLista,
                tareaseleccionada: state.tareaseleccionada,
                obtenerTareas,
                agregarTarea,
                eliminarTarea,
                guardarTareaActual,
                actualizarTarea,
                limpiarTarea
            }}
        >
            {props.children}
        </TareaContext.Provider>
    )
}

export default TareaState;